package com.anxpp.calculator.bio;

import com.anxpp.utils.Calculator;

import java.io.*;
import java.net.Socket;

/**
 *  客户端线程，用于处理一个客户端的Socket链路
 *  @author lijun
 *  @version 1.0
 */
public class ServerHandler implements Runnable{
    private Socket socket;
    public ServerHandler(Socket socket){
        this.socket = socket;
    }

    /**
     * 此种方式，貌似可以正常工作，但是有如下不好的地方
     * (1).自定义接受缓存，１２８字节字符数组，这个限制死了，超出了怎么办？？？
     * (2).接收的字符串，还包括了最后的\n换行符，需要自己去除
     */
//    public void run(){
//        InputStream in = null; // 流：客户端->服务端（读）
//        OutputStream out = null;
//        try {
//            in = socket.getInputStream();
//            out = socket.getOutputStream(); // 流：服务端->客户端（写）
//            int receiveBytes;
//            byte[] receiveBuffer = new byte[128];//此处有一个128的限制,使用buffer就没有这个限制
//            String clientMessage = "";
//            if((receiveBytes=in.read(receiveBuffer))!=-1) {
//                clientMessage = new String(receiveBuffer, 0, receiveBytes);
//                clientMessage=clientMessage.replace("\r","").replace("\n","");
////                System.out.println("服务器收到的消息：" + clientMessage);
//                String strResult = Calculator.conversion(clientMessage);
//                System.out.println("服务器收到的消息：" + clientMessage+",计算结果为" + strResult+",准备返回给客户端.");
//                out.write(strResult.getBytes());
//            }
//            out.flush();
//            out.close();
////            System.out.println("Server: receives clientMessage->" + clientMessage);
//        } catch (IOException e) {
//            e.printStackTrace();
//            if( in!=null) {
//                try {
//                    in.close();
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                }
//                in=null;
//            }
//            if(out!=null) {
//                try {
//                    out.close();
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                }
//                out=null;
//            }
//        }
//    }

    public void run(){
        BufferedReader in = null;
        PrintWriter out = null;
        try{
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
            String expression;
            String result;
            while(true){
                //通过BufferedReader读取一行
                //如果已经读到输入流尾部，返回null，退出循环
                //如果得到非空值，就尝试计算结果并返回
                if((expression = in.readLine()) == null) break;
                try{
                    result = Calculator.conversion(expression);
                }catch(Exception e){
                    result = "计算错误："+ e.getMessage();
                }
                System.out.println("服务器收到的消息：" + expression+",计算结果："+result);
                out.println(result);
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            //一些必要的清理工作
            if(in != null){
                try{
                    in.close();
                }catch(IOException e){
                    e.printStackTrace();
                }
                in = null;
            }
            if(out != null){
                out.close();
                out = null;
            }
            if(socket != null){
                try{
                    socket.close();
                }catch(IOException e){
                    e.printStackTrace();
                }
                socket = null;
            }
        }

    }


}

