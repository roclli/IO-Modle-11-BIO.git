---------------------------------------------------------------------------------------
mvn clean compile exec:java -Dexec.mainClass="com.anxpp.calculator.bio.Server"
mvn compile exec:java -Dexec.mainClass="com.anxpp.calculator.bio.Client"
---------------------------------------------------------------------------------------
注意：
(1).伪异步的服务器+BIO的客户端(一个服务器(开２个线程)，９个客户端)
    CPU Load average: (基本上8core都在80%左右,停止以后12%左右)   9.79 8.40 6.44
    Memory          : 13.0G/15.6G       (停止以后10.3G/15.6G)
(2).NIO的服务器+BIO的客户端(一个服务器，９个客户端)
    CPU Load average: (基本上8core都在68%左右,停止以后12%左右)   6.22 8.52 7.12
    Memory          : 13.8G/15.6G       (停止以后10.8G/15.6G)
(3).BIO的服务器+BIO的客户端(一个服务器，９个客户端)
    CPU Load average: (基本上8core都在60%左右,停止以后12%左右)   7.03 7.63 6.78
    Memory          : 14.4G/15.6G       (停止以后10.3G/15.6G)
(4).AIO的服务器+BIO的客户端(一个服务器，９个客户端)
    CPU Load average: (基本上8core都在46%左右,停止以后12%左右)   5.72 5.80 5.47
    Memory          : 12.4G/15.6G       (停止以后10.5G/15.6G)

分析：
(1).按照CPU由高到底：     伪异步 >  NIO > BIO   >  AIO
(2).按照内存消耗由高到底：  BIO  >  NIO > 伪异步 >  AIO
---------------------------------------------------------------------------------------
本文是BIO编程模型(同步阻塞式I/O)：

采用BIO通信模型的服务端，通常由一个单独的Acceptor线程负责监听客户端的连接，
它接收到客户端连接请求之后为每个客户端创建一个新的线程进行链路处理没处理完成后，
通过输出流返回应答给客户端，线程销毁。即典型的一请求一应答通宵模型。


1、BIO编程
(1.1)、传统的BIO编程---同步通信模型---同步阻塞I/O服务端通信模型(客户端-线程)
(1.2)、伪异步I/O编程---伪异步I/O模型--同步阻塞I/O服务端通信模型(N客户端M线程，N可以远大于M)


2、什么是同步,什么是异步
    同步和异步是针对应用和内核的交互而言.
    同步指的是用户进程触发IO操作并等待或者轮询去查看IO是否就绪;
    异步指的是用户进程触发IO操作以后便开始做自己的事情,IO操作已完成的时候会得到IO完成的通知.
    同步必须等待或者主动的去询问IO是否完成,这其实就是同步和异步最关键的区别

    阻塞和非阻塞是针对进程在访问数据的时候,根据IO操作的就绪状态采取不同的方式,
    阻塞方式下读取或者写入函数将一直等待,非阻塞方式下,读取或者写入函数会立即返回一个状态值.
3、同步/异步是宏观上(进程间通讯,通常表现为网络IO的处理上),阻塞/非阻塞是微观上
    (进程内数据传输,通常表现为对本地IO的处理上)fuse和非阻塞是同步/异步的表现形式.

4、“一个IO操作＂其实分成了两个步骤：发起IO请求和实际的IO操作。
    同步IO和异步IO的区别就在于第二个步骤是否阻塞，如果实际的IO读写阻塞请求进程，那么就是同步IO。
    阻塞IO和非阻塞IO的区别在于第一步，发起IO请求是否会被阻塞，如果阻塞直到完成那么就是传统的阻塞IO，
        如果不阻塞，那么就是非阻塞IO。

跑了一会之后会报错：
java.net.NoRouteToHostException: Cannot assign requested address (Address not available)
        at java.net.PlainSocketImpl.socketConnect(Native Method)
        at java.net.AbstractPlainSocketImpl.doConnect(AbstractPlainSocketImpl.java:350)
        at java.net.AbstractPlainSocketImpl.connectToAddress(AbstractPlainSocketImpl.java:206)
        at java.net.AbstractPlainSocketImpl.connect(AbstractPlainSocketImpl.java:188)
        at java.net.SocksSocketImpl.connect(SocksSocketImpl.java:392)
        at java.net.Socket.connect(Socket.java:589)
        at java.net.Socket.connect(Socket.java:538)
        at java.net.Socket.<init>(Socket.java:434)
        at java.net.Socket.<init>(Socket.java:211)
        at com.anxpp.calculator.bio.Client.send(Client.java:30)
        at com.anxpp.calculator.bio.Client.send(Client.java:21)
        at com.anxpp.calculator.bio.Client.main(Client.java:69)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:498)
        at org.codehaus.mojo.exec.ExecJavaMojo$1.run(ExecJavaMojo.java:282)
        at java.lang.Thread.run(Thread.java:748)
因为端口用尽，但是端口不是立即释放， 而是处于TIME_WAIT状态， 默认等待60s后才释放

经网上查资料， 是由于linux分配的客户端连接端口用尽， 无法建立socket连接所致，虽然socket正常关闭，但是端口不是立即释放， 而是处于TIME_WAIT状态， 默认等待60s后才释放。
    查看linux支持的客户端连接端口范围, 也就是28232个端口：
        cat  /proc/sys/net/ipv4/ip_local_port_range
        32768 - 61000

    解决方法：
    1. 调低端口释放后的等待时间， 默认为60s， 修改为15~30s
        echo 30 > /proc/sys/net/ipv4/tcp_fin_timeout
    2. 修改tcp/ip协议配置， 通过配置/proc/sys/net/ipv4/tcp_tw_resue, 默认为0， 修改为1， 释放TIME_WAIT端口给新连接使用。
        echo 1 > /proc/sys/net/ipv4/tcp_tw_reuse
    3. 修改tcp/ip协议配置，快速回收socket资源，  默认为0， 修改为1.
        echo 1 > /proc/sys/net/ipv4/tcp_tw_recycle




